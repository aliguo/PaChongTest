package com.example.demo;

import org.apache.poi.hssf.usermodel.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author huoChangGuo
 * @since 2019/7/30
 */
public class ExportExcelUtils {
    private static final Logger logger = LoggerFactory.getLogger(ExportExcelUtils.class);

    /**
     * 导出Excel
     *
     * @param excelName 要导出的excel名称
     * @param list      要导出的数据集合
     * @param response  使用response可以导出到浏览器
     * @param <T>
     */
    public static <T> void export(String excelName, List<Map<String, String>> list, HttpServletResponse response) {

        // 设置默认文件名为当前时间：年月日时分秒
        if (excelName == null || excelName == "") {
            excelName = new SimpleDateFormat("yyyyMMddhhmmss").format(
                    new Date()).toString();
        }
        // 设置response头信息
        response.reset();
        response.setContentType("application/vnd.ms-excel"); // 改成输出excel文件
        try {
            response.setHeader("Content-disposition", "attachment; filename="
                    + new String(excelName.getBytes("gb2312"), "ISO-8859-1") + ".xls");
        } catch (UnsupportedEncodingException e1) {
            logger.info(e1.getMessage());
        }

        try {
            //创建一个WorkBook,对应一个Excel文件
            HSSFWorkbook wb = new HSSFWorkbook();
            //在Workbook中，创建一个sheet，对应Excel中的工作薄（sheet）
            HSSFSheet sheet = wb.createSheet(excelName);
            //创建单元格，并设置值表头 设置表头居中
            HSSFCellStyle style = wb.createCellStyle();
            // 填充工作表
            fillSheet(sheet, list, style);

            //将文件输出
            OutputStream ouputStream = response.getOutputStream();
            wb.write(ouputStream);
            ouputStream.flush();
            ouputStream.close();
        } catch (Exception e) {
            logger.info("导出Excel失败！");
            logger.error(e.getMessage());
        }
    }


    /**
     * 向工作表中填充数据
     *
     * @param sheet excel的工作表名称
     * @param list  数据源
     * @param style 表格中的格式
     * @throws Exception 异常
     */
    public static <T> void fillSheet(HSSFSheet sheet, List<Map<String, String>> list, HSSFCellStyle style) throws Exception {
        logger.info("向工作表中填充数据:fillSheet()");

        //在sheet中添加表头第0行,注意老版本poi对Excel的行数列数有限制short
        HSSFRow row = sheet.createRow((int) 0);

        // 填充表头
        HSSFCell cell0 = row.createCell(0);
        cell0.setCellValue("Name");
        cell0.setCellStyle(style);
        HSSFCell cell1 = row.createCell(1);
        cell1.setCellValue("Price");
        cell1.setCellStyle(style);
        HSSFCell cell2 = row.createCell(2);
        cell2.setCellValue("Company");
        cell2.setCellStyle(style);
        HSSFCell cell3 = row.createCell(3);
        cell3.setCellValue("Keyword");
        cell3.setCellStyle(style);

        int rowIndex = 1;
        for (Map<String, String> item : list) {
            row = sheet.createRow(rowIndex);
            row.createCell(0).setCellValue(item.get("Name"));
            row.createCell(1).setCellValue(item.get("Price"));
            row.createCell(2).setCellValue(item.get("Company"));
            row.createCell(3).setCellValue(item.get("Keyword"));
            rowIndex++;
        }
    }
}
