package com.example.demo.controller;

import com.example.demo.PaChong;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author huoChangGuo
 * @since 2019/7/30
 */
@Controller
@RequestMapping("")
public class MainController {
    private static final Logger logger = LoggerFactory.getLogger(MainController.class);

    /**
     * 首页
     *
     * @return
     */
    @GetMapping("")
    private String index() {
        return "index";
    }

    /**
     * 导出
     *
     * @param request
     * @param response
     */
    @RequestMapping("running")
    @ResponseBody
    private void running(HttpServletRequest request, HttpServletResponse response) {
        String keyword = request.getParameter("keyword");
        int pageStart = Integer.valueOf(request.getParameter("pageStart"));
        int pageEnd = Integer.valueOf(request.getParameter("pageEnd"));
        try {
            PaChong.running(keyword, pageStart, pageEnd, response);
        } catch (Exception e) {
            logger.error("running", e);
        }

    }
}
