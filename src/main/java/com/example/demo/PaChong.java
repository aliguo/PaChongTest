package com.example.demo;

import org.apache.commons.lang.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletResponse;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * @author huoChangGuo
 * @since 2019/7/18
 */
public class PaChong {

    private static final Logger logger = LoggerFactory.getLogger(PaChong.class);
    private Document docAll;
    private Document inDoc;


    private List<Map<String, String>> paAliBaBa(String keyword, int pageStart, int pageEnd) {
        int currentPage = pageStart;
        List<Map<String, String>> list = new ArrayList<>();
        Map<String, String> dataMap = new HashMap<>();

        ExecutorService fixPool = Executors.newFixedThreadPool(10);

        CountDownLatch countDownLatch = new CountDownLatch(pageEnd - pageStart + 1);

        while (currentPage <= pageEnd) {
            int finalCurrentPage = currentPage;
            fixPool.execute(new Runnable() {
                @Override
                public void run() {
                    docAll = null;
                    inDoc = null;
                    Elements allTitle = new Elements();
                    String mainUrl = "https://www.alibaba.com/products/" + keyword + ".html?IndexArea=product_en&viewtype=L&n=62&page=" + finalCurrentPage;
                    try {
                        docAll = Jsoup.connect(mainUrl).get();
                        allTitle = docAll.select("h2.title");
                    } catch (IOException e) {
                        logger.error("爬取列表{} 第{}页炸了", e, finalCurrentPage);
                    }
                    for (Element itemTitle : allTitle) {
                        Map<String, String> dataMap = new HashMap<>();
                        Elements titleA = itemTitle.select("a");
                        String href = titleA.attr("href");
                        dataMap.put("href", href);
                        String proName = titleA.text();
                        dataMap.put("Name", proName);
                        Elements priceItem = itemTitle.parent().select("div.title-wrap").nextAll().select("div.pmo-kv-wrap").select("div.price").select("b");
                        Elements companyItem = itemTitle.parents().select("div.item-col").nextAll().select("div.item-extra").select("a.list-item__minisite-link");
                        if (priceItem.size() > 0) {
                            String proPrice = priceItem.get(0).text();
                            dataMap.put("Price", proPrice);
                        } else {
                            dataMap.put("Price", "未搜索到");
                        }
                        if (companyItem.size() > 0) {
                            String proCom = companyItem.get(0).text();
                            dataMap.put("Company", proCom);
                        } else {
                            dataMap.put("Company", "未搜索到");
                        }
                        list.add(dataMap);
                    }
                    logger.info("第" + finalCurrentPage + "页已完成");
                    countDownLatch.countDown();
                }
            });
            currentPage++;
        }
        //当所有线程执行完，唤醒，不然阻塞
        try {
            countDownLatch.await();
            fixPool.shutdown();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        // 详情页线程池
        ExecutorService fixPoolDetail = Executors.newFixedThreadPool(100);
        CountDownLatch countDownLatchDetail = new CountDownLatch(list.size());
        logger.info("爬取详情页............................................");
        for (Map<String, String> item : list) {

            fixPoolDetail.execute(new Runnable() {
                @Override
                public void run() {
                    String href = item.get("href");
                    String keywordContent = "未搜索到";
                    if (!StringUtils.isEmpty(href)) {
                        try {
                            inDoc = Jsoup.connect("https:" + href).get();
                            keywordContent = (inDoc.select("meta[name=keywords]")).attr("content");
                        } catch (IOException e) {
                            logger.error("爬取详情", e);
                        }
                    }
                    item.put("Keyword", keywordContent);
                    countDownLatchDetail.countDown();
                }
            });

        }
        //当所有线程执行完，唤醒，不然阻塞
        try {
            countDownLatchDetail.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return list;
    }

    public static String running(String keyword, int pageStart, int pageEnd, HttpServletResponse response) {
        PaChong pt = new PaChong();
        logger.info("搜集数据中....");
        List<Map<String, String>> list = pt.paAliBaBa(keyword, pageStart, pageEnd);
        logger.info("共" + list.size() + "条,写入数据中....");
        String fileName = "KEYWORD_" + keyword.trim() + "_PAGE_" + pageEnd + ".xls";
        try {
            ExportExcelUtils.export(fileName, list, response);
        } catch (Exception e) {
            logger.error("导出文件", e);
        }
        logger.info("完成");
        return fileName;
    }
}
