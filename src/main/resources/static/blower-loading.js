"use strict";
/*
 Author: Jafar Akhondali
 Company: DioStudio
 Release date: 2016, 24 Jul
 Title:  Jquery plugin + svg animated loading bar
 Design idea:
 Thanks to @joomlachannel
 */
var LoadingBlower = (function () {
  function LoadingBlower(selector) {
    this.intervalValue = 0;
    this.intervalValueRote = 0;
    this.fillerWidth = 0;
    this.progress = 0;
    this.blowerDom = $(selector);
    this.filler = $(this.blowerDom.find(".filler")[0]);
    this.markerContainer = this.blowerDom.find(".marker_container");
    this.progressDom = this.blowerDom.find(".value");
  }


  LoadingBlower.prototype.setProgress = function (progress, everyPercent) {
    var diff = progress - this.progress;
    this.addProgress(diff, everyPercent);
  };
  LoadingBlower.prototype.clear = function () {
    this.filler.css('width', 0 + "%");
    this.progressDom.html(0 + "%");
    this.intervalValue = 0;
    this.fillerWidth = 0;
    this.progress = 0;
  };
  LoadingBlower.prototype.addProgress = function (dif, everyPercent) {
    if (this.progress === 100 && dif >= 0)
      return;
    this.progress = dif + this.progress > 100 ? 100 : dif + this.progress;
    this.intervalValue === 0 ? this.intervalCheck(this.progress,everyPercent) : 0;
  };
  LoadingBlower.prototype.intervalCheck = function (totle,everyPercent) {
    var _this = this;
    this.intervalValue = setInterval(function () {
      _this.fillerWidth += 1;
      if (_this.fillerWidth > totle) {
        clearInterval(_this.intervalValue);
        _this.intervalValue = 0;
      }else{
        console.log(_this.fillerWidth);
        _this.filler.css('width', _this.fillerWidth + "%");
        _this.progressDom.html(_this.fillerWidth + "%");
      }
    }, everyPercent);
  };
  LoadingBlower.prototype.rotate = function () {
    var _this = this;
    var count = 0, degrees = 40;
    _this.intervalValueRote = setInterval(function () {
      _this.markerContainer.css({
        '-webkit-transform': 'rotate(' + degrees * count + 'deg)',
        '-moz-transform'   : 'rotate(' + degrees * count + 'deg)',
        '-ms-transform'    : 'rotate(' + degrees * count + 'deg)',
        '-o-transform'     : 'rotate(' + degrees * count + 'deg)',
        'transform'        : 'rotate(' + degrees * count + 'deg)',
        'zoom'             : 1
      });
      count++;
    }, 100);
  };
  LoadingBlower.prototype.clearRotate = function () {
    var _this = this;
    clearInterval(_this.intervalValue);
    clearInterval(_this.intervalValueRote);
    _this.markerContainer.css({
      '-webkit-transform': 'rotate(' + 0 + 'deg)',
      '-moz-transform'   : 'rotate(' + 0 + 'deg)',
      '-ms-transform'    : 'rotate(' + 0 + 'deg)',
      '-o-transform'     : 'rotate(' + 0 + 'deg)',
      'transform'        : 'rotate(' + 0 + 'deg)',
      'zoom'             : 1
    });
  };
  return LoadingBlower;
}());
